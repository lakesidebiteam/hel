﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hel.Models
{
    public class UserModel
    {
        private int _employeeid;
        private string _employeename;
        private string _jobtitle;
        private int _companyid;
        private string _companyname;
        private string _division;
        private string _divisiondescription;
        private string _divisionabbreviation;
        private string _trade;
        private string _username;
        private string _email;
        private string _workphone;
        private string _personalphone;
        private int _supervisorid;
        private string _supervisorname;
        private string _imagefilename;
        private DateTime _logintimestamp;

        [Key]
        public int EmployeeID
        {
            get { return _employeeid; }
            set { _employeeid = value; }
        }

        public string EmployeeName { get; set; }
        public string JobTitle { get; set; }
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string Division { get; set; }
        public string DivisionDescription { get; set; }
        public string DivisionAbbreviation { get; set; }
        public string Trade { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string WorkPhone { get; set; }
        public string PersonalPhone { get; set; }
        public int SupervisorID { get; set; }
        public string SupervisorName { get; set; }
        public string ImageFileName { get; set; }
        [Timestamp]
        public DateTime LoginTimeStamp { get; }
    
    }
}