﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hel.Models.Asphalt
{
    [Table("Specifications", Schema = "Asphalt")]
    public class SpecificationsModel
    {
        [Key]
        public int SpecificationID { get; set; }
        public int ProjectionID { get; set; }
        [Required]
        public int PlantID { get; set; }
        [Required]
        public int LiquidTypeID { get; set; }
        [Required]
        public float LiquidPercent { get; set; }
        [Required]
        public int SourceID { get; set; }
        [Required]
        public int StatusID { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        [Timestamp]
        public DateTime AuditTimestamp { get; }

        public virtual ProjectionsModel Projections { get; set; }
        public ICollection<TonsModel> Tons { get; set; }
    }
}