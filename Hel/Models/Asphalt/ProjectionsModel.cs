﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hel.Models.Asphalt
{
    [Table("Projections", Schema = "Asphalt")]
    public class ProjectionsModel
    {
        [Key]
        public int ProjectionID { get; set; }
        [Required]
        public int ProjectionTypeID { get; set; }
        [Required]
        public string Division { get; set; }
        [Required]
        public string Project { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int StatusID { get; set; }
        [Required]
        [DisplayName("Awarded Tons")]
        public int AwardedTons { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        //[Timestamp]
        public DateTime AuditTimestamp { get; }

        public ICollection<SpecificationsModel> Specifications { get; set; }
    }
}