﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hel.Models.Asphalt
{
    [Table("Tons", Schema = "Asphalt")]
    public class TonsModel
    {
        [Key]
        public int TonID { get; set; }
        [ForeignKey("Specifications")]
        public int SpecificationID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime ProjectedDate { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        [Timestamp]
        public DateTime AuditTimestamp { get; }
        public virtual SpecificationsModel Specifications { get; set; }

    }
}