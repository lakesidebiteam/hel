﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hel.Models.Asphalt
{
    [Table("ReviewLog", Schema = "Asphalt")]
    public class ReviewLogModel
    {
        [Key]
        public int ReviewLogID { get; set; }
        public int EmployeeID { get; set; }
        public string UserName { get; set; }
        [Timestamp]
        public DateTime ReviewDate { get; }
    }
}