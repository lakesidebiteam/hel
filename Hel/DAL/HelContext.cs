﻿using Hel.Models;
using Hel.Models.Asphalt;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Hel.DAL
{
    public class HelContext :DbContext
    {
        public HelContext() : base("HelContext")
        {
        }

        public DbSet<ProjectionsModel> Projections { get; set; }
        public DbSet<SpecificationsModel> Specifications { get; set; }
        public DbSet<TonsModel> Tons { get; set; }
        public DbSet<UserModel> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}