﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hel.DAL;
using Hel.Models.Asphalt;

namespace Hel.Controllers
{
    public class ProjectionsController : Controller
    {
        private HelContext db = new HelContext();

        // GET: Projections
        public async Task<ActionResult> Index()
        {
            return View(await db.Projections.ToListAsync());
        }

        // GET: Projections/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //ProjectionsModel projections = await db.Projections.FindAsync(id);
            string query = "SELECT * FROM Asphalt.Projections WHERE ProjectionID = @p0"; 
            ProjectionsModel projection = await db.Projections.SqlQuery(query, id).SingleOrDefaultAsync();
            if (projection == null)
            {
                return HttpNotFound();
            }
            return View(projection);
        }

        // GET: Projections/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Projections/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProjectionID,ProjectionTypeID,Division,Project,Description,StatusID,AwardedTons,AuditID")] ProjectionsModel projections)
        {
            if (ModelState.IsValid)
            {
                db.Projections.Add(projections);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(projections);
        }

        // GET: Projections/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectionsModel projections = await db.Projections.FindAsync(id);
            if (projections == null)
            {
                return HttpNotFound();
            }
            return View(projections);
        }

        // POST: Projections/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProjectionID,ProjectionTypeID,Division,Project,Description,StatusID,AwardedTons,AuditID")] ProjectionsModel projections)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projections).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(projections);
        }

        // GET: Projections/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProjectionsModel projections = await db.Projections.FindAsync(id);
            if (projections == null)
            {
                return HttpNotFound();
            }
            return View(projections);
        }

        // POST: Projections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProjectionsModel projections = await db.Projections.FindAsync(id);
            db.Projections.Remove(projections);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
