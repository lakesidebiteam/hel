﻿namespace Asgard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    //public class ReferenceModel
    //{
    //    public ICollection<AsphaltSources> AsphaltSources { get; set; }
    //    public ICollection<DivisionDefaults> DivisionDefaults { get; set; }
    //    public ICollection<LiquidTypes> LiquidTypes { get; set; }
    //    public ICollection<ProductionType> ProductionType { get; set; }
    //    public ICollection<Status> Status { get; set; }
    //}

    [Table("AsphaltSources", Schema = "Ref")]
    public class AsphaltSources
    {
        [Key]
        public int AsphaltSourceID { get; }
        public string AsphaltSource { get; set; }
        public int SortOrder { get; set; }
    }

    [Table("DivisionDefaults", Schema = "Ref")]
    public class DivisionDefaults
    {
        [Key]
        public int Division { get; }
        public int PlantKey { get; set; }
        public int LiquidTypeID { get; set; }
        public float AsphaltPercent { get; set; }
    }

    [Table("LiquidTypes", Schema = "Ref")]
    public class LiquidTypes
    {
        [Key]
        public int LiquidTypeID { get; }
        public string LiquidType { get; set; }
        public int SortOrder { get; set; }
    }

    [Table("PlantDefaults", Schema = "Ref")]
    public class PlantDefaults
    {
        [Key]
        public int LocationKey { get; }
        public float AsphaltPercent { get; set; }
    }

    [Table("ProductionType", Schema = "Ref")]
    public class ProductionType
    {
        [Key]
        public int ProductionTypeID { get; }
        public string Description { get; set; }
    }

    [Table("Status", Schema = "Ref")]
    public class Status
    {
        [Key]
        public int StatusID { get; }
        public string Description { get; set; }
    }
}
