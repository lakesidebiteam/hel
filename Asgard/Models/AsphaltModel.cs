﻿namespace Asgard.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Threading.Tasks;

    //public class AsphaltModel
    //{
    //    public ICollection<Production> Production { get; set; }
    //    public ICollection<Specification> Specification { get; set; }
    //    public ICollection<Tons> Tons { get; set; }
    //    public ICollection<ReviewLog> ReviewLog { get; set; }
    //}

    [Table("Production", Schema = "Asphalt")]
    public class Production
    {
        [Key]
        public int ProductionID { get; set; }
        [Required]
        public int ProductionTypeID { get; set; }
        [Required]
        public string Division { get; set; }
        [Required]
        public string Project { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int StatusID { get; set; }
        [Required]
        [DisplayName("Awarded Tons")]
        public int AwardedTons { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        [Timestamp]
        public DateTime AuditTimestamp { get; }
        public ICollection<Specification> Specifications { get; set; }
        public ICollection<Tons> Tons { get; set; }
    }

    [Table("Specification", Schema = "Asphalt")]
    public class Specification
    {
        [Key]
        public int SpecificationID { get; set; }
        [ForeignKey("Production")]
        public int ProductionID { get; set; }
        [Required]
        public int PlantID { get; set; }
        [Required]
        public int LiquidTypeID { get; set; }
        [Required]
        public float LiquidPercent { get; set; }
        [Required]
        public int SourceID { get; set; }
        [Required]
        public int StatusID { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        [Timestamp]
        public DateTime AuditTimestamp { get; }
        public ICollection<Tons> Tons { get; set; }
    }

    [Table("Tons", Schema = "Asphalt")]
    public class Tons
    {
        [Key]
        public int TonID { get; set; }
        [ForeignKey("Specification")]
        public int SpecificationID { get; set; }
        [ForeignKey("Production")]
        public int ProductionID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime ProjectedDate { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public int AuditID { get; set; }
        [Required]
        [Timestamp]
        public DateTime AuditTimestamp { get; }
    }

    [Table("ReviewLog", Schema = "Asphalt")]
    public class ReviewLog
    {
        [Key]
        public int ReviewLogID { get; set; }
        public int EmployeeID { get; set; }
        public string UserName { get; set; }
        [Timestamp]
        public DateTime ReviewDate { get; }
    }
}
