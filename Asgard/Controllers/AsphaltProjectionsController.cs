﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Asgard.Data;
using Asgard.Models;
using System.Data.SqlClient;
using System.DirectoryServices;

namespace Asgard.Controllers
{
    public class AsphaltProjectionsController : Controller
    {
        private readonly AsgardContext _context;

        public AsphaltProjectionsController(AsgardContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {

            ViewData["Title"] = "Asphalt Projections";
            return View(await _context.Productions.ToListAsync());
        }


    }
}