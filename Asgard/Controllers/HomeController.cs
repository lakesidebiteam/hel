﻿using Asgard.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.DirectoryServices;
using Asgard.Data;
using System.Data;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private AsgardContext _context;
        private UserModel _userModel;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public HomeController(AsgardContext context)
        { 
            _context = context;
        }

        public IActionResult Index()
        {
            ViewData["User"] = _userModel.LoadUserData(User.Identity.Name, _context);
            ViewData["Title"] = "bob";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}