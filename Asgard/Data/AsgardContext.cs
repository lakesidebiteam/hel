﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Asgard.Models;

namespace Asgard.Data
{
    public class AsgardContext : DbContext
    {
        public AsgardContext (DbContextOptions<AsgardContext> options)
            : base(options)
        {
        }

        public AsgardContext()
        {
        }

        public DbSet<Asgard.Models.Production> Productions { get; set; }
        public DbSet<Asgard.Models.Specification> Specifications { get; set; }
        public DbSet<Asgard.Models.UserModel> UserModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Asgard.Models.Production>().ToTable("Production");
            modelBuilder.Entity<Asgard.Models.Specification>().ToTable("Specification");
            modelBuilder.Entity<Asgard.Models.UserModel>().ToTable("UserModel");
        }

    }
}
